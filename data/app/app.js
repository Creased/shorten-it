'use strict';

/*!
 * Author: Baptiste MOINE <contact@bmoine.fr>
 * Project: Shorten-it
 * Homepage: https://go.bmoine.fr/
 * Released: 02/06/2017
 * Documentation: https://app.swaggerhub.com/apis/Creased/shorten-it/0.0.1-a
 */

// Modules
var express     = require('express'),
    app         = express(),
    bodyparser  = require('body-parser').json(),
    server      = require('http').createServer(app),
    redis       = require('redis'),
    fs          = require('fs'),
    path        = require('path'),
    logger      = require('morgan'),
    bs58        = require('bs58check'),
    request     = require('request');

var json = function(res, code, data) {
    res.writeHead(( (code !== 0) ? code : 200 ), {
        'Content-Type': 'application/json; charset=utf-8'
    });

    data = (typeof data === "string") ? data : JSON.stringify(data);

    res.write(data);
    res.end();
};

var hash = function(id) {
    return bs58.encode(new Buffer(parseInt(id).toString()));
}

var rand = function(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

// Application configuration
var config = require('./config.json'),
    default_config = {
        'redis': {      // Redis server
            'host':     process.env.REDIS_HOST || '127.0.0.1',
            'port':     process.env.REDIS_PORT || 6379
        },
        'app': {        // Listening host and port
            'host':     process.env.APP_HOST   || '0.0.0.0',
            'port':     process.env.APP_PORT   || 8080,
            'env':      process.env.NODE_ENV   || 'development',
            'base_url': process.env.BASE_URL   || 'https://go.bmoine.fr/'
        }
    };

config.redis.host      = config.redis.host    || default_config.redis.host
config.redis.port      = config.redis.port    || default_config.redis.port
config.app.host        = config.app.host      || default_config.app.host
config.app.port        = config.app.port      || default_config.app.port
config.app.env         = config.app.env       || default_config.app.env
config.app.base_url    = config.app.base_url  || default_config.app.base_url

// Redis client
var client = redis.createClient(config.redis.port, config.redis.host);

// Body parser
app.use(bodyparser);

// Logging
app.use(logger('combined', {
    stream: fs.createWriteStream(path.join(__dirname, '/logs/access.log'), {flags: 'a'})    // Create a write stream (in append mode)
}));
(config.app.env !== 'development') ? app.use(logger('combined')) : app.use(logger('dev'));  // Console stream

// Public files
app.use(express.static('public'));

// Index
app.get('/', function (req, res) {
    res.render('index.ejs');
});

// Redirector
app.get('/:short_url(*)', function (req, res) {
    var short_url = req.params.short_url.split('/')[0].split('?')[0].split('#')[0],
        long_url;
    var regex = new RegExp('^/' + short_url, 'i');
    var url_params = req.url.replace(regex, '');

    client.hmget(short_url, 'long_url', function (err, data) {
        if (!err && data[0] !== null) {
            long_url = data[0];
            res.status(301);
            res.set('Location', long_url + url_params);
            res.send();
        } else {
            json(res, 404, 'No such URL in database');
        }
    });
});

// Shortener
app.post('/', function(req, res){
    request.get(req.body.url, function (checkerr, checkres, checkdata) {
        if (!checkerr && ( typeof checkres.statusCode !== 'undefined' && checkres.statusCode == 200 )) {
            var id = rand(1, 0xFFFFFF);  // Max: 0xFFFFFFFFFFFFF
            var short_url = hash(id);
            var data = {
                'created_at': new Date(),
                'long_url': req.body.url
            };

            client.hmset(short_url, data);

            data['short_url'] = config.app.base_url + short_url;

            json(res, 200, data);
        } else {
            json(res, 500, {
                'status': 'An error occured while checking submitted URL'
            });
        }
    });
});

// Start server
server.listen(config.app.port, config.app.host, function () {
    console.log("Server is listening on %s:%d", config.app.host, config.app.port)
});
