function select(input) {
    var input = $(input) || $(this);
    input.focus();
    input.select();
}

function serialzeObject(form) {
    var arr = form.serializeArray(),
        obj = {};

    jQuery.map(arr, function(id){
        obj[id['name']] = id['value'];
    });

    return obj;
}

$(document).ready(function() {
    args = {
        shortURLOutput:   '#short_url',
        longURLInput:     '#long_url',
        qrOutput:         '#short_url_qr',
        form:             '#mainForm'
    };

    select(args.longURLInput);

    function successRequest(data) {
        var url = data.short_url;
        var options = {
            render:     'image',
            ecLevel:    'H',
            minVersion: 10,
            fill:       '#3e3e3e',
            background: '#f9f9f9',
            text:       url,
            size:       400,
            radius:     0.5,
            quiet:      4
        };

        $(args.shortURLOutput).val(url).prop('type', 'url').css('display', 'block');
        $(args.qrOutput).empty().qrcode(options).css('display', 'block');
    }

    $(args.shortURLOutput).click(function() {
        select(this);
    });

    $(args.form).submit(function(event) {
        var form = $(args.form);

        event.preventDefault();

        jQuery.ajax({
            type: form.attr("method"),
            url: form.attr("action"),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            data: JSON.stringify(serialzeObject(form)),
            async: true,
            resetForm: false,
            success: successRequest
        }, "json");

        return false;
    });
});
